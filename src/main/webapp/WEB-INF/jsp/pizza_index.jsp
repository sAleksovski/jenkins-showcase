<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ristes
  Date: 11/9/15
  Time: 5:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order Form</title>
</head>
<body>

<h1>Order pizza</h1>

<h3>Select pizza size:</h3>

<form title="Select size" action="clientInfo" method="post">
    <c:forEach var="size" items="${pizzaTypes}">
        <label>
            <input type="radio" name="pizzaType" value="${size}" required/>
            ${size}
        </label>
        <br/>
    </c:forEach>
    <input type="submit" value="Submit">
</form>

<script type="text/javascript">
  if (window.location.href[window.location.href.length-1] != '/') {
    window.location.href += '/';
  }
</script>

</body>
</html>
