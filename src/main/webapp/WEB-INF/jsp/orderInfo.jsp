<%--
  Created by IntelliJ IDEA.
  User: stefan
  Date: 11/19/15
  Time: 8:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order info</title>
</head>
<body>
<h1>Confirmation</h1>
<p>Name: ${order.clientName}</p>
<p>Address: ${order.clientAddress}</p>
<p>Pizza size: ${order.pizzaType}</p>
<p>User-Agent: ${header["user-agent"]}</p>
</body>
</html>
