package mk.ukim.finki.wp.web;

import mk.ukim.finki.wp.model.Order;
import mk.ukim.finki.wp.service.OrderService;
import mk.ukim.finki.wp.service.PizzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * Created by stefan on 11/19/15.
 */
@Controller
public class PizzaOrderController {

    @Autowired
    PizzaService pizzaService;

    @Autowired
    OrderService orderService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index() {
        return new ModelAndView("pizza_index", "pizzaTypes", pizzaService.getPizzaTypes());
    }

    @RequestMapping(value = "clientInfo", method = RequestMethod.POST)
    public ModelAndView showClientInfo(@RequestParam String pizzaType, HttpSession session) {
        session.setAttribute("pizzaType", pizzaType);
        return new ModelAndView("clientInfo");
    }

    @RequestMapping(value = "order", method = RequestMethod.POST)
    public ModelAndView placeOrder(@RequestParam String clientName, @RequestParam String clientAddress, HttpSession session) {
        Order order = orderService.placeOrder(session.getAttribute("pizzaType").toString(), clientName, clientAddress);
        return new ModelAndView("orderInfo", "order", order);
    }

}
