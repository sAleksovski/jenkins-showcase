package mk.ukim.finki.wp.service.impl;

import mk.ukim.finki.wp.model.Order;
import mk.ukim.finki.wp.service.OrderService;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * Created by stefan on 11/19/15.
 */
@Service
public class OrderServiceImpl implements OrderService {
    public Order placeOrder(String pizzaType, String clientName, String address) {
        Random r = new Random();
        return new Order(pizzaType, clientName, address, r.nextLong());
    }
}
