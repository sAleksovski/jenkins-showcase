package mk.ukim.finki.wp.service.impl;

import mk.ukim.finki.wp.service.PizzaService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Created by stefan on 11/19/15.
 */
@Service
public class PizzaServiceImpl implements PizzaService {
    public List<String> getPizzaTypes() {
        return Arrays.asList("Small", "Medium", "Large");
    }
}
