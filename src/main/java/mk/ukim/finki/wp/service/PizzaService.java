package mk.ukim.finki.wp.service;

import java.util.List;

/**
 * Created by stefan on 11/19/15.
 */
public interface PizzaService {
    public List<String> getPizzaTypes();
}
