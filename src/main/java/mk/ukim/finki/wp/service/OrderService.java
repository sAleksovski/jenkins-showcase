package mk.ukim.finki.wp.service;

import mk.ukim.finki.wp.model.Order;

/**
 * Created by stefan on 11/19/15.
 */
public interface OrderService {
    public Order placeOrder(String pizzaType, String clientName, String address);
}
